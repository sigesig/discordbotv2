import Discord from 'discord.js'

/**
 * Used to keep track of when the Distube instance is paused
 */
export module pausing {
    let paused = false;

    /**
     * Used to check if the bot is currently paused
     */
    export function isPaused() {
        return paused;
    }

    /**
     *  Change pause state(flip boolean value)
     */
    export function changePauseState() {
        paused = !paused;
    }
}