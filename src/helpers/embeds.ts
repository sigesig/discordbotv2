import Discord from "discord.js";

/**
 * Embedded section, methods here are for creating embeds. So that some embeds are streamlined like Error message look a like, etc.
 */
export module embeds {
    /**
     * Function to create a Discord embed to send when an error occurs:
     * Color is red
     * @param errorMessage - Description of the error
     * @param cause - What caused it which command caused it and what params, etc.
     * @param footer - Information to be added in the footer
     * @return Creates a Discord embed usually used for error embeds
     */
    export function createErrorEmbed(errorMessage: string, cause?: string, footer?: string): Discord.MessageEmbed {
        const embed = new Discord.MessageEmbed()
            .setColor('#ff0000')
            .setTitle("An error occurred")
            .setDescription(errorMessage)
            .setTimestamp()
        if(cause){
            embed.addField("Cause of error", cause)
        }
        if(footer){
            embed.setFooter({text: footer})
        }

        return embed
    }

    /**
     * Function to create a Discord embed to send when some simple information needs to be shared:
     * Color is blue
     * @param embedTitle - Title of the embed
     * @param information - The information itself which is added to the description
     * @param footer - Information to be added in the footer
     * @return Creates a Discord embed usually used for giving information
     */
    export function createSimpleInfoEmbed(embedTitle: string, information: string, footer?: string): Discord.MessageEmbed {
        const embed = new Discord.MessageEmbed()
            .setColor('#0099FF')
            .setTitle(embedTitle)
            .setDescription(information)
            .setTimestamp()

        if(footer){
            embed.setFooter({text: footer})
        }

        return embed
    }

    /**
     * Function to create a Discord embed to send when something succeeds:
     * Color is green
     * @param embedTitle - Title of the embed
     * @param footer - Information to be added in the footer
     * @return Creates a Discord embed usually used for giving information when something is successfully
     */
    export function createSuccessEmbed(embedTitle: string, footer?: string): Discord.MessageEmbed {
        const embed = new Discord.MessageEmbed()
            .setColor('#30a101')
            .setTitle(embedTitle)
            .setTimestamp()

        if(footer){
            embed.setFooter({text: footer})
        }

        return embed
    }

    /**
     * Function to create a Discord embed to send a simple warning:
     * Color is yellow
     * @param information - The information itself which is added to the description
     * @param footer - Information to be added in the footer
     * @return Creates a Discord embed usually used for giving a warning
     */
    export function createSimpleWarning(information: string, footer?: string){
        const embed = new Discord.MessageEmbed()
            .setColor('#ffff00')
            .setDescription(information)
            .setTimestamp()

        if(footer){
            embed.setFooter({text: footer})
        }

        return embed
    }

    /**
     * Function to create a Discord embed to send a simple Error:
     * Color is red
     * @param information - The information itself which is added to the description
     * @param title - Title of the embed
     * @param footer - Information to be added in the footer
     * @return Creates a Discord embed usually used for showing an issue
     */
    export function createSimpleIssue(information: string, title?: string, footer?: string): Discord.MessageEmbed{
        const embed = new Discord.MessageEmbed()
            .setColor('#ff0000')
            .setDescription(information)
            .setTimestamp()

        if(footer){
            embed.setFooter({text: footer})
        }
        if (title){
            embed.setTitle(title)
        }

        return embed
    }
}

/**
 * Interactable Embedded section, methods here are for creating interactable embeds. So that some Interactable embeds are streamlined like for a yes, no questions.
 */
export module InteractableEmbeds{

    /**
     * Used to create accept/decline component used for embeds
     */
    export function acceptDeclineComponent(): Discord.MessageActionRow{
        const buttonRow = new Discord.MessageActionRow()
        buttonRow.addComponents(
            new Discord.MessageButton()
                .setCustomId('Accept')
                .setStyle('SUCCESS')
                .setEmoji('✅')
        )
        buttonRow.addComponents(
            new Discord.MessageButton()
                .setCustomId('Decline')
                .setStyle('DANGER')
                .setEmoji('🛑')
        )
        return buttonRow
    }
}