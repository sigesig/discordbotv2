import {reply} from "../helpers/messages";
import {embeds} from "../helpers/embeds";

//Embed tests
test('Test error message gets set to description', () =>{
    let errorMessage = "Test error message"
    expect(embeds.createErrorEmbed(errorMessage).description).toBe(errorMessage)
})
test('Test error cause gets set to a field', () =>{
    let errorMessage = "Test error message"
    let errorCause = "Rip bad code"
    expect(embeds.createErrorEmbed(errorMessage, errorCause).fields[0].value).toBe(errorCause)
})

