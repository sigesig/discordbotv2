import { ICommand } from "wokcommands"
import {deletes, duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {distube} from "../../main";
import {VoiceBasedChannel} from "discord.js";
import {channelChecks} from "../../helpers/validationChecks";
import deleteMessageAfterTime = deletes.deleteMessageAfterTime;
import {pausing} from "../../helpers/music";

export default {
    names: "pause",
    category: 'Music',
    description: 'Pauses the current playing media', // Required for slash commands
    maxArgs: 0,
    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds

    callback: ({ message, interaction, args }) => {
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;

        // message is provided for a legacy command
        if (message) {
            message.react("👌")
            if (!distube.getQueue(message)?.playing) return;
            distube.pause(message)
            pausing.changePauseState()
            deleteMessageAfterTime(message, duration.ThirtySeconds)
            return
        }
        // interaction is provided for slash commands
        if (!distube.getQueue(interaction)?.playing) return;
        distube.pause(interaction)
        pausing.changePauseState()
        reply.replyToInteraction(interaction,"Media paused",undefined, duration.ThirtySeconds)
    },
} as ICommand

