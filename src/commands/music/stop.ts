import { ICommand } from "wokcommands"
import {deletes, duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {distube} from "../../main";
import {channelChecks} from "../../helpers/validationChecks";

export default {
    names: "stop",
    category: 'Music',
    description: 'Stop the everything music related', // Required for slash commands
    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds

    callback: ({ message, interaction }) => {
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;

        // message is provided for a legacy command
        if (message) {
            // Check queue
            if (distube.getQueue(message) == undefined){
                reply.replyToMessage(message, embeds.createErrorEmbed("Nothing is playing"),undefined, duration.ThirtySeconds,true)
                return;
            }

            // @ts-ignore
            if (distube.getQueue(message).paused) {
                reply.replyToMessage(message, embeds.createErrorEmbed("Nothing is playing"),undefined, duration.ThirtySeconds,true)
            }

            try{
                message.react("👌")
                distube.stop(message);
                deletes.deleteMessageAfterTime(message,duration.ThirtySeconds)
            }
            catch(err){
                reply.replyToMessage(message, embeds.createErrorEmbed("Something went wrong"),undefined, duration.OneMinute, true)
            }
            return
        }
        // interaction is provided for slash commands
        // Check queue
        if (distube.getQueue(interaction) == undefined){
            reply.replyToInteraction(interaction, embeds.createErrorEmbed("Nothing is playing"),undefined, duration.ThirtySeconds)
            return;
        }

        // @ts-ignore
        if (distube.getQueue(interaction).paused) {
            reply.replyToInteraction(interaction, embeds.createErrorEmbed("Nothing is playing"),undefined, duration.ThirtySeconds)
        }


        try{
            distube.stop(interaction);
            const returnEmbed = embeds.createSimpleWarning("Toggling shuffle")
            reply.replyToInteraction(interaction, returnEmbed,undefined,duration.OneMinute)
        }
        catch(err){
            reply.replyToInteraction(interaction, embeds.createErrorEmbed("Something went wrong"),undefined, duration.OneMinute)
        }

    },
} as ICommand

