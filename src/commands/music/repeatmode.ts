import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {distube} from "../../main";
import {VoiceBasedChannel} from "discord.js";
import {channelChecks} from "../../helpers/validationChecks";
import {RepeatMode} from "distube";

export default {
    names: "Repeat mode",
    category: 'Music',
    description: 'Set the repeat mode of the queue, repeat mode, 0=Disabled, 1=Repeat song, 2=Repeat queue', // Required for slash commands
    aliases: ['repeat','loop'],
    maxArgs: 1,
    expectedArgs: '<mode>',
    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds
    options: [
        {
            name: 'mode', // Must be lower case
            description: 'A number represents a mode, 0=Disabled, 1=Repeat song, 2=Repeat queue',
            required: false,
            type: 'NUMBER',
        },
    ],

    callback: ({ message, interaction, args }) => {
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;
        let mode;
        const errorEmbed = embeds.createErrorEmbed("Invalid repeat mode")
        const returnEmbed = embeds.createSimpleInfoEmbed("Repeat Mode","Set repeat mode to `" + mode + "`")

        // message is provided for a legacy command
        if (message) {

            // Check that the repeat mode is correct
            if (args[0] != undefined){
                if (parseInt(args[0]) < 0 && parseInt(args[0]) > 2 ){
                    reply.replyToMessage(message,errorEmbed,undefined,duration.ThirtySeconds,true)
                }
            }

            // Check queue
            if (distube.getQueue(message) == undefined){
                reply.replyToMessage(message, embeds.createErrorEmbed("Nothing is playing"),undefined, duration.ThirtySeconds,true)
                return;
            }

            try{
                switch(distube.setRepeatMode(message, parseInt(args[0]))) {
                    case RepeatMode.DISABLED:
                        mode = "Off";
                        break;
                    case RepeatMode.SONG:
                        mode = "Repeat a song";
                        break;
                    case RepeatMode.QUEUE:
                        mode = "Repeat all queue";
                        break;
                }
                reply.replyToMessage(message,returnEmbed,undefined,duration.OneMinute,true)
            }
            catch(err){
                reply.replyToMessage(message, embeds.createErrorEmbed("Something went wrong"),undefined, duration.OneMinute, true)
            }
            return
        }
        // interaction is provided for slash commands

        // Check that the repeat mode is correct
        if (args[0] != undefined){
            if (parseInt(args[0]) < 0 && parseInt(args[0]) > 2 ){
                reply.replyToMessage(message,errorEmbed,undefined,duration.ThirtySeconds,true)
            }
        }
        // Check queue
        if (distube.getQueue(interaction) == undefined){
            reply.replyToInteraction(interaction, embeds.createErrorEmbed("Nothing is playing"),undefined, duration.ThirtySeconds)
            return;
        }

        try{
            switch(distube.setRepeatMode(interaction, parseInt(args[0]))) {
                case RepeatMode.DISABLED:
                    mode = "Off";
                    break;
                case RepeatMode.SONG:
                    mode = "Repeat a song";
                    break;
                case RepeatMode.QUEUE:
                    mode = "Repeat all queue";
                    break;
            }
            reply.replyToInteraction(interaction, returnEmbed,undefined, duration.OneMinute)
        }
        catch(err){
            reply.replyToInteraction(interaction, embeds.createErrorEmbed("Something went wrong"),undefined, duration.OneMinute)
        }

    },
} as ICommand

