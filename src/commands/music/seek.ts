import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {distube} from "../../main";
import {VoiceBasedChannel} from "discord.js";
import {channelChecks} from "../../helpers/validationChecks";
import {parseNumber} from "distube";

export default {
    names: "seek",
    category: 'Music',
    description: 'Set the playing time to another position', // Required for slash commands
    minArgs: 1,
    maxArgs: 1,
    expectedArgs: '<number>',
    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds
    options: [
        {
            name: 'position', // Must be lower case
            description: 'playing time in seconds',
            required: true,
            type: 'NUMBER',
        },
    ],

    callback: ({ message, interaction, args }) => {
        if (!channelChecks.voiceChannelChecks(message, interaction)) return;

        const errorEmbed = embeds.createErrorEmbed("Invalid position")


        // message is provided for a legacy command
        if (message) {
            if(distube.getQueue(message)?.songs !== undefined){
                // @ts-ignore
                if(parseNumber(args[0]) >= distube.getQueue(message).songs[0].duration){
                    reply.replyToMessage(message, errorEmbed,undefined, duration.OneMinute, true)
                    return;
                }
            }

            try{
                message.react("👌")
                distube.seek(message, parseInt(args[0]))
            }
            catch(err){
                reply.replyToMessage(message, errorEmbed,undefined, duration.OneMinute, true)
            }

            return
        }
        // interaction is provided for slash commands

        if(distube.getQueue(interaction)?.songs !== undefined){
            // @ts-ignore
            if(parseNumber(args[0]) >= distube.getQueue(interaction).songs[0].duration){
                reply.replyToInteraction(interaction, errorEmbed,undefined, duration.OneMinute)
            }
        }

        try{
            distube.seek(interaction, parseInt(args[0]))
            reply.replyToInteraction(interaction, embeds.createSimpleWarning("Playing from " + args[0] + " seconds"),undefined, duration.TenSeconds)
        }
        catch(err){
            reply.replyToInteraction(interaction, errorEmbed,undefined, duration.OneMinute)
        }

    },
} as ICommand

