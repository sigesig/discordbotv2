import { ICommand } from "wokcommands"
import {deletes, duration, reply} from "../../helpers/messages";
import {embeds} from "../../helpers/embeds";
import {misc} from "../../helpers/misc";
import deleteMessageAfterTime = deletes.deleteMessageAfterTime;

export default {
    names: "vækEnEmo",
    aliases: ['taber'],
    category: 'General',
    description: 'Moves a person around for a given amount or until they are back', // Required for slash commands
    minArgs: 1,
    permissions: ['MOVE_MEMBERS'],
    expectedArgs: '<name>',
    slash: 'both', // Create both a slash and legacy command
    testOnly: false, // Only register a slash command for the testing guilds
    options: [
        {
            name: 'name', // Must be lower case
            description: 'user you want to moved around',
            required: true,
            type: 'STRING',
        },
    ],

    callback: ({ message, interaction,args }) => {
        const replyMsg = embeds.createSimpleInfoEmbed("Ping!","Legacy reply Ping!")
        // message is provided for a legacy command TODO: FIX SLOW REJOIN ORIGINAL
        if (message) {
            if (!message.guild){
                return
            }
            message.guild.members.fetch({ query: args.join(" "), limit: 1 })
                .then(async members => {
                    if (members.size <= 0) {
                        reply.replyToMessage(message, embeds.createSimpleIssue("Couldn't find a user with that name"),undefined, duration.ThirtySeconds, true)
                    }

                    let notInChannelEmbed = embeds.createSimpleIssue("User isn't in a voice channel")

                    let memberId = members.firstKey()
                    // @ts-ignore
                    let member = await message.guild.members.fetch(memberId)

                    // @ts-ignore
                    let channels = await message.guild.channels.fetch();
                    //Find Voice channels and non afk channel
                    let voiceChannels= channels.filter(c => c.type === "GUILD_VOICE" && c.id !== '256089230655094785').map(channel => channel.id)
                    // @ts-ignore
                    if (!member.voice.channel) {
                        reply.replyToMessage(message, notInChannelEmbed,undefined, duration.ThirtySeconds, true)
                        return;
                    }

                    message.react("👌")
                    deleteMessageAfterTime(message, duration.ThirtySeconds)

                    if (member.user.bot){
                        reply.replyToMessage(message, embeds.createErrorEmbed("Never try to move a bot, we are superior. Get moved","Won't move the master race"),undefined,duration.OneMinute,true)
                        let numberOfMoves = 5
                        let messageCreator = message.member
                        // @ts-ignore
                        let originalVoiceChannel = [message.member.voice.channel.id];
                        for (let i = 0; i < numberOfMoves; i++) {
                            // @ts-ignore
                            let currentVoiceChannel = messageCreator.voice.channel.id;
                            // @ts-ignore
                            await misc.moveToRandomVoiceChannel(voiceChannels, currentVoiceChannel, messageCreator)

                            if (i == numberOfMoves - 1){
                                console.log(i +" test " + numberOfMoves)
                                // @ts-ignore
                                await misc.moveToRandomVoiceChannel(originalVoiceChannel, currentVoiceChannel, messageCreator)
                            }
                        }
                        return
                    }


                    let numberOfMoves = 10
                    let originalVoiceChannel = [member.voice.channel.id];
                    for (let i = 0; i < numberOfMoves; i++) {
                        // @ts-ignore
                        let currentVoiceChannel = member.voice.channel.id;
                        await misc.moveToRandomVoiceChannel(voiceChannels, currentVoiceChannel, member)
                        if (i == numberOfMoves - 1){
                            await misc.moveToRandomVoiceChannel(originalVoiceChannel, currentVoiceChannel, member)
                        }
                    }

                })
                .catch(() =>{reply.replyToMessage(message,embeds.createErrorEmbed("Something went wrong"),undefined,duration.OneMinute,true)});
            return
        }
        // interaction is provided for slash commands
        if (!interaction.guild){
            return
        }
        interaction.guild.members.fetch({ query: args.join(" "), limit: 1 })
            .then(async members => {
                if (members.size <= 0) {
                    reply.replyToInteraction(interaction, embeds.createSimpleIssue("Couldn't find a user with that name"),undefined, duration.ThirtySeconds)
                }

                let notInChannelEmbed = embeds.createSimpleIssue("User isn't in a voice channel")

                let memberId = members.firstKey()
                // @ts-ignore
                let member = await interaction.guild.members.fetch(memberId)

                // @ts-ignore
                let channels = await interaction.guild.channels.fetch();
                //Find Voice channels and non afk channel
                let voiceChannels= channels.filter(c => c.type === "GUILD_VOICE" && c.id !== '256089230655094785').map(channel => channel.id)
                // @ts-ignore
                if (!member.voice.channel) {
                    reply.replyToInteraction(interaction, notInChannelEmbed,undefined, duration.ThirtySeconds)
                    return;
                }

                if (member.user.bot){
                    reply.replyToInteraction(interaction, embeds.createErrorEmbed("Never try to move a bot, we are superior. Get moved","Won't move the master race"),undefined,duration.OneMinute)
                    let numberOfMoves = 5
                    let messageCreator = interaction.member
                    // @ts-ignore
                    let originalVoiceChannel = [interaction.member.voice.channel.id];
                    for (let i = 0; i < numberOfMoves; i++) {
                        // @ts-ignore
                        let currentVoiceChannel = messageCreator.voice.channel.id;
                        // @ts-ignore
                        await misc.moveToRandomVoiceChannel(voiceChannels, currentVoiceChannel, messageCreator)
                        if (i == numberOfMoves - 1){
                            // @ts-ignore
                            await misc.moveToRandomVoiceChannel(originalVoiceChannel, currentVoiceChannel, messageCreator)
                        }
                    }
                    return
                }


                let numberOfMoves = 10
                let originalVoiceChannel = [member.voice.channel.id];
                reply.replyToInteraction(interaction, embeds.createSimpleWarning("I'll yeet them around"),undefined,duration.OneMinute)
                for (let i = 0; i < numberOfMoves; i++) {
                    // @ts-ignore
                    let currentVoiceChannel = member.voice.channel.id;
                    await misc.moveToRandomVoiceChannel(voiceChannels, currentVoiceChannel, member)
                    if (i == numberOfMoves - 1){
                        await misc.moveToRandomVoiceChannel(originalVoiceChannel, currentVoiceChannel, member)
                    }
                }
            })

    },
} as ICommand