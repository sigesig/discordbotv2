import { ICommand } from "wokcommands"
import {duration, reply} from "../../helpers/messages";
import {embeds, InteractableEmbeds} from "../../helpers/embeds";
import Discord, {CacheType, Interaction, InteractionCollector, Message, MessageComponentInteraction} from "discord.js";
import {models} from "../../database/models";
import {misc} from "../../helpers/misc";
import {databaseInteractions} from "../../database/databaseInteractions";
//TODO make this use Text Input	instead of a direct command
export default {
    names: "addaccount",
    aliases: ['addacc'],
    category: 'Accounts',
    description: 'Add an account to the database', // Required for slash commands
    minArgs: 3,
    expectedArgs: '<username><password><game>',
    slash: 'both', // Create both a slash and legacy command
    guildOnly: true,
    testOnly: false, // Only register a slash command for the testing guilds
    options: [
        {
            name: 'username', // Must be lower case
            description: 'Username for this account',
            required: true,
            type: 'STRING',
        },
        {
            name: 'password', // Must be lower case
            description: 'Password for this account',
            required: true,
            type: 'STRING',
        },
        {
            name: 'game', // Must be lower case
            description: 'Name of the game please use the full name i.e \"league of legends\"',
            required: true,
            type: 'STRING',
        },
    ],
    callback: async ({user, message, interaction, args, channel}) => {
        let gameName = args.slice(2, undefined).join(' ').toLowerCase()
        const replyEmbed = embeds.createSimpleWarning(`**Username:** \`${args[0].toLowerCase()}\`\n**Password:** \`${args[1]}\`\n**Game:** \`${gameName}\``).setTitle("Are these values correct?")
        const replyMsgComponent = InteractableEmbeds.acceptDeclineComponent()

        let msg: Discord.Message
        const id = user.id
        let collector: InteractionCollector<MessageComponentInteraction<CacheType>>
        const filter = (i: Interaction) => i.user.id === user.id
        const time = duration.OneMinute

        // message is provided for a legacy command
        if (message) {
            msg = await reply.replyToMessage(message, replyEmbed,[replyMsgComponent], duration.OneMinute, true)
            collector = msg.createMessageComponentCollector({ filter, time})
        } else {
            // interaction is provided for slash commands
            reply.replyToInteraction(interaction, replyEmbed,[replyMsgComponent], duration.OneMinute, true)
            collector = channel.createMessageComponentCollector({ filter, time})
        }

        collector.on('collect', async (btnInt) => {
            if (!btnInt) {
                return
            }

            btnInt.deferUpdate()
            // Check if some random button was pressed not accept or decline
            if (btnInt.customId !== 'Accept' && btnInt.customId !== 'Decline') {
                return
            }

            // Handle if accept was pressed
            if (btnInt.customId === 'Accept') {
                replyMsgComponent.components[0].setDisabled(true)
                replyMsgComponent.components[1].setDisabled(true)
                if (message) {
                    msg.edit({components: [replyMsgComponent]})
                } else {
                    interaction.editReply({components: [replyMsgComponent]})
                }
                collector.stop()

                // Check if account already exists on database
                let alreadyExists = await databaseInteractions.fetchDocuments(models.accountModel.AccountModel,{username: args[0].toLowerCase(), game: gameName})
                if (alreadyExists !== undefined) {
                    let existEmbed = embeds.createSimpleIssue("This account already exists for this game, if you want to change it please use the \`editaccount\` command", "Account already exists")
                    msg.edit({embeds: [existEmbed]})
                    return
                }
                //Create document
                let doc = new models.accountModel.AccountModel({
                    username: args[0].toLowerCase(),
                    password: args[1],
                    game: gameName,
                    databaseId: misc.generateUUID(),
                    lastEditedBy: user.username
                })

                // Create document on database
                let result = await databaseInteractions.createDocument(doc)
                console.log(result)
                if (result) {
                    let succesEmbed = embeds.createSuccessEmbed("Account was successfully added")
                    msg.edit({embeds: [succesEmbed], components: undefined})
                } else {
                    let errorEmbed = embeds.createErrorEmbed("Something went wrong while adding to database")
                    msg.edit({embeds: [errorEmbed], components: undefined})
                }

            }
            // Handle if accept was pressed
            if (btnInt.customId === 'Decline') {
                replyMsgComponent.components[0].setDisabled(true)
                replyMsgComponent.components[1].setDisabled(true)
                if (message) {
                    msg.edit({components: [replyMsgComponent]})
                } else {
                    interaction.editReply({components: [replyMsgComponent]})
                }
                collector.stop()
            }

        })

    },
} as ICommand
