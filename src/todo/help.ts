import { ICommand } from 'wokcommands'

export default {

    category: 'Testing',
    description: 'Replies with pong', // Required for slash commands

    callback: ({ instance }) => {
        instance.commandHandler.commands.forEach((command: any) => {
            console.log(command)

        })
    },
} as ICommand