import Discord, {Intents} from 'discord.js'
import { DisTube } from "distube";
import dotenv from 'dotenv'
import path from 'path'
import WOKCommands from 'wokcommands'
import {duration, reply, sendMessage} from './helpers/messages'
import {embeds} from "./helpers/embeds";
import SoundCloudPlugin from "@distube/soundcloud";
import SpotifyPlugin from "@distube/spotify";
import { YtDlpPlugin } from "@distube/yt-dlp";

dotenv.config()

//Prefix for commands that the bot should read
let prefix = "%"
if(process.env.PREFIX != undefined){
    prefix = process.env.PREFIX
}

//Initialize discord bot
const bot = new Discord.Client({
    intents: [
        Intents.FLAGS.GUILDS,
        Intents.FLAGS.GUILD_VOICE_STATES,
        Intents.FLAGS.GUILD_MESSAGES,
        Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
        Intents.FLAGS.GUILD_PRESENCES,
        Intents.FLAGS.GUILD_VOICE_STATES,
    ]
})


let ClientID = process.env.SPOTIFYAPPCLIENTID
let Secret = process.env.SPOTIFYAPPCLIENTSECRET
/**
 * Distube instance
 */
export const distube = new DisTube(bot, {
    searchSongs: 10,
    searchCooldown: 0,
    leaveOnEmpty: false,
    leaveOnFinish: false,
    leaveOnStop: false,
    emitNewSongOnly: true,
    youtubeDL: false,
    emitAddSongWhenCreatingQueue: false,
    emitAddListWhenCreatingQueue: false,
    plugins: [new SoundCloudPlugin(),
        new SpotifyPlugin({
            parallel: true,
            emitEventsAfterFetching: false,
            api: {
                clientId: ClientID || '',
                clientSecret: Secret || '',
            },
        }),
        new YtDlpPlugin({ update: true })
    ]
})

//Called once bot is started up
bot.on('ready', async () => {
    console.log('Ready!');
    //Typescript thought i needed this!?
    if (bot.isReady()) {
        //Setup bot status
        bot.user.setPresence({
            activities: [{
                name: prefix + 'help',
                type: 'LISTENING',
            }],
            status: 'online'
        });
    }
    let uri = "mongodb://"+ process.env.MONGODB_USER + ":" + process.env.MONGODB_PW + "@mongo:27017/"
    //WOK setup
    new WOKCommands(bot, {
        mongoUri: uri,
        dbOptions: {
            keepAlive: true
        },
        // The name of the local folder for your command files
        commandsDir: path.join(__dirname, 'commands'),
        // The name of the local folder for your features files
        featuresDir: path.join(__dirname, 'features'),
        // Allow importing of .ts files if you are using ts-node
        typeScript: true,
        botOwners: ['256807585804582912'],
        testServers: ['770399479450042409', '769832316503326720']
    })
        .setDefaultPrefix(prefix)
        .setCategorySettings([
            {
                name: "Music",
                emoji: "🎧"
            },
            {
                name: 'Fun & Games',
                emoji: '🎮'
            },
            {
                name: 'Testing',
                emoji: '🏗',
                hidden: true
            },
            {
                name: 'Configuration',
                emoji: '🚧',
                hidden: true
            },
            {
                name: 'General',
                emoji: '🛒',
            },
            {
                name: 'Shitposting',
                emoji: '🚽',
            },
            {
                name: 'Accounts',
                emoji: '📎',
            },
        ])

});

// Queue status template
// @ts-ignore
const status = (queue) => {
    return `Volume: \`${queue.volume}%\` | Filter: \`${queue.filter || "Off"}\` | Loop: \`${queue.repeatMode ? queue.repeatMode == 2 ? "All Queue" : "This Song" : "Off"}\` | Autoplay: \`${queue.autoplay ? "On" : "Off"}\``;
};
// DisTube event listeners
distube.on("playSong", (queue, song) => {
        let add = `Playing \`${song.name}\` - \`${song.formattedDuration}\`\nRequested by: ${song.user}\n${status(queue)}`;
        const playEmbed = embeds.createSimpleWarning(add);

        // @ts-ignore
        sendMessage.sendMessageUsingQueue(queue, playEmbed,undefined, (song.duration + 1) * 1000)
})
    .on("addSong", (queue, song) =>{
        let add = `Added ${song.name} - \`${song.formattedDuration}\` to the queue by ${song.user}`
        const addEmbed = embeds.createSimpleWarning(add)
        sendMessage.sendMessageUsingQueue(queue, addEmbed,undefined, duration.ThirtySeconds)
    })
    .on("addList", (queue, playlist) => {
        let add = `Added \`${playlist.name}\` playlist (${playlist.songs.length} songs) to queue\n${status(queue)}`
        const addEmbed = embeds.createSimpleWarning(add)
        sendMessage.sendMessageUsingQueue(queue, addEmbed,undefined, duration.ThirtySeconds)
    })
    .on('error', (channel, error) => {
        console.error(error)
        const errorEmbed = embeds.createErrorEmbed("error.name",'${error.slice(0, 1979)}')// Discord limits 2000 characters in a message
        sendMessage.sendMessageToChannel(channel, errorEmbed,undefined, duration.OneMinute)
    })
    .on("disconnect", queue => {
        const discEmbed = embeds.createSimpleWarning("Disconnected")
        sendMessage.sendMessageUsingQueue(queue, discEmbed,undefined, duration.OneMinute)
    })

//----------Command handling ----------
bot.on('messageCreate', async (message) => {
    if (!message.content.startsWith(prefix) || message.author.bot) return;
});


//Login in the bot
function loginBot() {
    bot.login(process.env.TOKEN)
}
loginBot()


//Used for CordeJS testing
exports.bot = bot;
exports.loginBot = loginBot;